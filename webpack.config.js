
const { merge } = require('webpack-merge');
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const { dependencies, name, version } = require('./package.json');
const Dotenv = require('dotenv-webpack');
// eslint-disable-next-line no-unused-vars
const dotenv = require('dotenv').config();
const commonConfig = {
    module: {
      rules: [
        {
          test: /\.m?js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-react', '@babel/preset-env'],
                plugins: ['@babel/plugin-transform-runtime']
              }
            }
          ]
        },
        {
          test: /\.(scss|css)$/,
          exclude: /node_modules\/(?!(@ftdr)\/).*/,
          use: ['style-loader', 'css-loader', 'sass-loader']
        }
      ]
    }
  };
const devConfig = {
  mode: 'development',
  devServer: {
    port: process.env.PORT,
    historyApiFallback: {
      index: 'index.html'
    }
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'insulter',
      filename: 'remoteEntry.js',
      exposes: {
        './Insulter': './src/bootstrap',
      },
      shared: dependencies
    }),
    new HtmlWebPackPlugin({
      template: './public/index.html'
    }),
    new Dotenv(),
    new webpack.EnvironmentPlugin({
      APP_GIT_SHA: 'git rev-parse --short HEAD',
      APP_VERSION: version,
      APP_NAME: name,
      INSULT_API_URI: JSON.stringify(process.env.INSULT_API_URI)
    })
  ]
};
module.exports = merge(commonConfig, devConfig);
