export const randomInsult = async (insulter) => {
    if (insulter === "random") {
        return fetchFields(`${process.env.INSULT_API_URI}/insults/random`);
    }
    else {
        return fetchFields(`${process.env.INSULT_API_URI}/insults/random?insulter=${insulter}`);
    }
};

export const getInsulters = async () => {
    return fetchData(`${process.env.INSULT_API_URI}/insulters`);
};

export const getInsulterImage = async (insulter) => {
    return fetchData (`${process.env.INSULT_API_URI}/insulters/image?insulter=${insulter}`);
};

export const fetchFields = async (url) => {
    return fetch(url)
    .then(response =>response.json())
    .then(response => response.data.fields)
    ;
};

export const fetchData = async (url) => {
    return fetch(url)
    .then(response =>response.json())
    .then(response => response.data)
    ;
};