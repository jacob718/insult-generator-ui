import React, { useEffect, useState } from 'react';
import { randomInsult, getInsulterImage } from '../services.js';
import './insulter.scss';
import Paper from '@material-ui/core/Paper';


const Insulter = (props) => {

    const { insulterSelect } = props;

    const [insult, setInsult] = useState();
    const [insultAgain, setInsultAgain] = useState(false);
    const [insulter, setInsulter] = useState("random");
    const [insulterName, setInsulterName] = useState(true);
    const [imageUrl, setImageUrl] = useState();

    useEffect(() => {
        if(insulterSelect != 'random') {
            setInsulter(insulterSelect);
            setInsulterName(insulterSelect);
        }
        else {
            setInsulter('random');
        }
    })

    useEffect(() => {
        getInsulterImage(insulterName)
            .then(response => {
                setImageUrl(response);
            });

            setInsultAgain(!insultAgain)
    }, [insulterName]);

    useEffect(() => {
        if (!insultAgain) {
            setInsult(insult)
        }
        else {
            randomInsult(insulter)
            .then(response => {
                setInsult(response.Insult);
                setInsulterName(response.Insulter);
                setInsultAgain(false);
            })
        }
        
    }, [insultAgain]);

    const InsulterLayout = () => {
        return (
        <div className="insulter">
            <div className="inner-insulter">
                <div className="photo">
                    <img src={imageUrl} alt="" />
                </div>
                
                <div className="body">
                    <div>
                        <div className="insult">
                            {insult}
                        </div>
                        
                        <div>
                            - {insulterName}
                        </div>
                    </div>
                    <div className="again">
                        <button className="btn" onClick={e => setInsultAgain(true)}>
                            Insult me again
                        </button>
                    </div>
                </div>
            </div>
        </div>
        
        )
    };

    return (
        <>
        
        <InsulterLayout></InsulterLayout>
        </>
    );
};

export default Insulter;