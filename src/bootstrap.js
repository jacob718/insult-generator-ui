import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { getInsulters } from './services';

const mount = (el, {options}, insulter) => { 
    ReactDOM.render(<App options={options} insulter={insulter} />, el);
}

if (process.env.NODE_ENV === 'development') { 
    const el = document.querySelector('#root');
    if (el) { 
        mount(el, { insulter: 'Luther'});
    }
}

export { mount, getInsulters };
