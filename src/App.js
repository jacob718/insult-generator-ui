import React from 'react';
import { createUseStyles } from 'react-jss';
import './styles/base-head.scss';
import Insulter from './components/insulter';
import {BrowserRouter as Router, Route } from 'react-router-dom';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const theme = createMuiTheme( {
    palette: {
        type: 'light'
    }
})


const App = ({ options, insulter }) => {
    if(!insulter) {
        insulter = "random";
    }
    return <>
        <ThemeProvider theme={theme}>
        <Insulter insulterSelect={insulter}/>
        </ThemeProvider>

    </>;
};


export default App;